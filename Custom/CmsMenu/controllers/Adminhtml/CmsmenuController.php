<?php

class Custom_CmsMenu_Adminhtml_CmsmenuController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
        $this->loadLayout()->_setActiveMenu('customcmsmenu');
        $this->_addContent($this->getLayout()->createBlock('customcmsmenu/adminhtml_cmsmenu'));
        $this->renderLayout();
    }
    
    public function newAction()
    {
        $this->_forward('edit');
    }
    
    public function editAction()
    {
        $id = (int) $this->getRequest()->getParam('id');
        $model = Mage::getModel('customcmsmenu/cmsmenu')->load($id);
        if($data = Mage::getSingleton('adminhtml/session')->getFormData()){
            $model->setData($data)->setId($id);
        } else {
            $model->load($id);
        }
        Mage::register('current_cmsmenu', $model);

        $this->loadLayout()->_setActiveMenu('customcmsmenu');
        $this->_addLeft($this->getLayout()->createBlock('customcmsmenu/adminhtml_cmsmenu_edit_tabs'));
        $this->_addContent($this->getLayout()->createBlock('customcmsmenu/adminhtml_cmsmenu_edit'));
        $this->renderLayout();
    }
    
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            try {
                $model = Mage::getModel('customcmsmenu/cmsmenu');
                $model->setData($data)->setId($this->getRequest()->getParam('id'));
                if(!$model->getCreated()){
                    $model->setCreated(now());
                }
                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Cms Menu was saved successfully'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array(
                    'id' => $this->getRequest()->getParam('id')
                ));
            }
            return;
        }
        Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                Mage::getModel('customcmsmenu/cmsmenu')->setId($id)->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Cms Menu was deleted successfully'));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $id));
            }
        }
        $this->_redirect('*/*/');
    }
    
    public function massDeleteAction()
    {
        $cmsmenu = $this->getRequest()->getParam('cmsmenu', null);
        if (is_array($cmsmenu) && sizeof($cmsmenu) > 0) {
            try {
                foreach ($cmsmenu as $id) {
                   
                    Mage::getModel('customcmsmenu/cmsmenu')->setId($id)->delete();
                }
                $this->_getSession()->addSuccess($this->__('Total of %d cmsmenu have been deleted', sizeof($cmsmenu)));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        } else {
            
            $this->_getSession()->addError($this->__('Please select menu'));
        }
        $this->_redirect('*/*');
    }

}