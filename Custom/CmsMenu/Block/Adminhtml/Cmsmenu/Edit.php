<?php

class Custom_CmsMenu_Block_Adminhtml_Cmsmenu_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    protected function _construct()
    {
        $this->_blockGroup = 'customcmsmenu';
        $this->_controller = 'adminhtml_cmsmenu';
        
         $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('adminhtml')->__('Save And Continue'),
            'onclick' => 'saveAndContinueEdit()',
            'class'   => 'save'
        ), -100);
         
         $this->_formScripts[] = " 
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }     
            ";
    }

    public function getHeaderText()
    {
        $helper = Mage::helper('customcmsmenu');
        $model = Mage::registry('current_cmsmenu');

        if ($model->getId()) {
            return $helper->__("Edit Menu '%s'", $this->escapeHtml($model->getMenuName()));
        } else {
            return $helper->__("New Link");
        }
    }

}