<?php

class Custom_CmsMenu_Block_Adminhtml_Cmsmenu_Edit_Tabs_Links extends Mage_Adminhtml_Block_Widget_Grid//Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('customcmsmenu/cmsmenu')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $helper = Mage::helper('customcmsmenu');

        $this->addColumn('links_id', array(
            'header' => $helper->__('ID'),
            'index'  => 'links_id'
        ));

        $this->addColumn('label', array(
            'header' => $helper->__('Label'),
            'index'  => 'label',
            'type'   => 'text',
        ));

        $this->addColumn('url_key', array(
            'header' => $helper->__('Url Key'),
            'index'  => 'url_key',
            'type'   => 'text',
        ));
        
        $this->addColumn('type', array(
            'header' => $helper->__('Type'),
            'index'  => 'type',
            'type'   => 'text',
        ));

        $this->addColumn('position', array(
            'header'  => $helper->__('Position'),
            'index'   => 'position',
            'type'    => 'options',
            'options' => array(
                'The Same Frame' => '1',
                'New Window'     => '2'
            ),
        ));
        
        return parent::_prepareColumns();
    }
    
}