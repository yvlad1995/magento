<?php

class Custom_CmsMenu_Block_Adminhtml_Cmsmenu extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    protected function _construct()
    {
        parent::_construct();

        $helper = Mage::helper('customcmsmenu');
        $this->_blockGroup = 'customcmsmenu';
        $this->_controller = 'adminhtml_cmsmenu';

        $this->_headerText = $helper->__('Manage Menus');
        $this->_addButtonLabel = $helper->__('Add News');
        
    }
    
}
