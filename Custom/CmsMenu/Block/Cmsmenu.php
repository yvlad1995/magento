<?php

class Custom_CmsMenu_Block_Cmsmenu extends Mage_Core_Block_Template
{

    public function getNewsCollection()
    {
        $cmsmenuCollection = Mage::getModel('customcmsmenu/cmsmenu')->getCollection();
        $cmsmenuCollection->setOrder('created', 'DESC');
        return $cmsmenuCollection;
    }

}